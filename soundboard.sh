#!/bin/bash
set -e

_firstline() {
	local line
	read line && echo -n "$line"
}

get_gdk_platform() {
	if [ ! -z "$WAYLAND_DISPLAY" ]; then
		echo -n wayland
	elif pidof Xorg; then
		echo -n xcb
	else
		echo -n xcb
	fi
}

verbosity=0
input_dirs=(~/.config/mcoffin-soundboard/sounds)
while getopts ':n:vi:' opt; do
	case ${opt} in
		n)
			name="$OPTARG"
			;;
		v)
			verbosity=$((verbosity + 1))
			;;
		i)
			input_dirs+=("$OPTARG")
			;;
		\?)
			echo "Unknown argument: -$OPTARG" >&2
			exit 1
			;;
		:)
			printf 'Invalid argument: -%s requires an argument\n' "$OPTARG" >&2
			exit 1
			;;
	esac
done
shift $((OPTIND - 1))

[ $verbosity -lt 3 ] || set -x

print_indented() {
	while [ $# -gt 0 ]; do
		printf '\t%s\n' "$1"
		shift
	done
}

if [ $verbosity -ge 1 ]; then
	echo -e '\033[1;36minput dirs\033[0m:' >&2
	print_indented "${input_dirs[@]}" >&2
fi

declare -a input_files
for d in "${input_dirs[@]}"; do
	input_files+=($(find "$d" -mindepth 1 -maxdepth 1 -type f))
done

if [ ${#input_files[@]} -lt 1 ]; then
	echo 'No input files found' >&2
	exit 1
fi

if [ $verbosity -ge 1 ]; then
	echo -e '\033[1;36mfiles\033[0m:' >&2
	for f in "${input_files[@]}"; do
		printf '\t%s\n' "$(basename "$f")" >&2
	done
fi

export GDK_PLATFORM="$(get_gdk_platform)"

choose_file() {
	local args=()
	local f
	for f in "${input_files[@]}"; do
		args+=("$(basename "$f")" "$f")
	done
	zenity --list \
		--title "Choose file to play" \
		--column Name \
		--column File \
		--print-column 2 \
		"${args[@]}" \
		| _firstline
}

soundboard_play() {
	local opt OPTIND OPTARG
	local name
	while getopts ':n:' opt; do
		case ${opt} in
			n)
				name="$OPTARG"
				;;
		esac
	done
	shift $((OPTIND - 1))
	if [ $# -lt 1 -o -z "$1" ]; then
		echo 'Usage: soundboard_play FILE' >&2
		return 1
	fi
	local f
	for f in "$@"; do
		# ffmpeg -i ~/.config/mcoffin-soundboard/sounds/boom-headshot.webm -filter:a loudnorm -f matroska - | mpv -
		ffmpeg -i "$f" -filter:a loudnorm -c:a libvorbis -f matroska - \
			| mpv --ao=jack --jack-name="${name:-soundboard}" -
	done
}

while [ true ]; do
	# set +e
	f="$(choose_file)"
	# [ $? -eq 0 ] || exit 0
	# set -e
	[ $verbosity -lt 1 ] || printf 'playing: \033[1;36m%s\033[0m\n' "$f" >&2
	soundboard_play "$f"
done
